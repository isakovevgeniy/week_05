package ru.edu;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.edu.model.Catalog;
import ru.edu.model.Registry;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;


public final class XMLRegistrySerializer implements RegistryExternalStorage {

    /**
     * Read file.
     * Parse file.
     * Create Catalog object.
     * @param filePath - path to XML File
     * @return Catalog object
     */
    public Catalog createCatalogFrom(final String filePath) {
        String line = "";
        Catalog catalog = new Catalog();
        try {
            line = readFrom(filePath);
            catalog = parseXMLFromLineToCatalog(line);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return catalog;
    }

    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     * @throws IOException Yes
     * @return line with XML data from file
     */
    private String readFrom(final String filePath) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(filePath)));
        while (br.ready()) {
            sb.append(br.readLine());
        }
        br.close();
        return sb.toString();
    }

    /**
     * XML parsing with Jackson.
     * @param line String with data in XML format
     * @return Catalog object
     * @throws JsonProcessingException Yes
     */
    private Catalog parseXMLFromLineToCatalog(final String line)
            throws JsonProcessingException {
        return new XmlMapper().readValue(line, Catalog.class);
    }


    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param registry реестр
     */
    @Override
    public void writeTo(final String filePath, final Registry registry) {
        File file = new File(filePath);
        XmlMapper mapper = new XmlMapper();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(file, registry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
