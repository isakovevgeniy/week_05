package ru.edu;

import ru.edu.model.Album;
import ru.edu.model.Artist;
import ru.edu.model.CD;
import ru.edu.model.Catalog;
import ru.edu.model.Country;
import ru.edu.model.Registry;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class RegistryFromCatalogCreator {
    /**
     * Объект типа Реестр. Реестр исполнителей.
     */
    private Registry registry;

    /**
     * Constructor.
     *
     * @param catalog ObjectType - Catalog
     */
    public RegistryFromCatalogCreator(final Catalog catalog) {
            this(catalog.getCdList());
    }

    /**
     * OverLoad constructor.
     *
     * @param catalog List of CD objects
     */
    public RegistryFromCatalogCreator(final List<CD> catalog) {
        Objects.requireNonNull(catalog,
                "Catalog = null. Please provide correct CD catalog.");
        setRegistry(catalog);
    }

    /**
     * Getter.
     *
     * @return Registry registry
     */
    public Registry getRegistry() {
        return registry;
    }

    /**
     * Getting the artist registry from the CD list.
     *
     * @param catalog List of CD objects
     */
    private void setRegistry(final List<CD> catalog) {
        List<Country> countries = getRegistryFromCatalog(catalog);
        registry = new Registry(countries);
    }

    /**
     * Creating Countries List from the CD list.
     * Fill Country Objects with Artists from CD list
     * Fill Artist Objects with Albums from CD list
     *
     * @param catalog List of CD objects
     * @return list of countries
     */
    private List<Country> getRegistryFromCatalog(final List<CD> catalog) {
        Map<String, Country> countries = catalog
                .stream()
                .map(cd -> new Country(cd.getCountry()))
                .distinct()
                .collect(Collectors
                        .toMap(Country::getName, country -> country));

        catalog.forEach(cd -> {
                    String countryName = cd.getCountry();
                    String artistName = cd.getArtist();
                    String albumName = cd.getTitle();
                    int albumYear = cd.getYear();

            Artist artist = addArtist(countries.get(countryName), artistName);
            addAlbum(artist, albumName, albumYear);
        });
        return new ArrayList<>(countries.values());
    }

    /**
     * Check Artist in country.
     * Create Artist if it doesn't exist
     *
     * @param country    Country object
     * @param artistName Artist name
     * @return Artist object
     */
    private Artist addArtist(final Country country,
                             final String artistName) {
        Artist artist = new Artist(artistName);
        if (!country.getArtists().contains(artist)) {
            country.getArtists().add(artist);
        }
        int artistIndex = country.getArtists().indexOf(artist);
        return country.getArtists().get(artistIndex);
    }

    /**
     * Check Album in Artists discography.
     * Create Album if it doesn't exist
     *
     * @param artist    Artist object
     * @param albumName Album name
     * @param year      release year
     */
    private void addAlbum(final Artist artist,
                          final String albumName,
                          final int year) {
        Album album = new Album(albumName, year);
        if (!artist.getAlbums().contains(album)) {
            artist.getAlbums().add(album);
        }
    }
}
