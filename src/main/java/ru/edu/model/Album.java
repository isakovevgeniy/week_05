package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.Objects;

public final class Album implements Serializable {
    /**
     * Album TITLE.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;
    /**
     * Album release year.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "year", isAttribute = true)
    private int year;

    /**
     * Что же это? Возможно это конструктор.
     *
     * @param pName
     * @param pYear
     */
    public Album(final String pName, final int pYear) {
        name = pName;
        year = pYear;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Album album = (Album) o;
        return year == album.year && name.equals(album.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, year);
    }

    @Override
    public String toString() {
        return "Name: " + name + " Year: " + year;
    }
}
