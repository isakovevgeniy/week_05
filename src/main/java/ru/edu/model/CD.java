package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

public final class CD implements Serializable {
    /**
     * Title.
     */
    @JsonProperty("TITLE")
    private String title;
    /**
     * Artist name.
     */
    @JsonProperty("ARTIST")
    private String artist;
    /**
     * Country name.
     */
    @JsonProperty("COUNTRY")
    private String country;
    /**
     * Company name.
     */
    @JsonProperty("COMPANY")
    private String company;
    /**
     * Price.
     */
    @JsonProperty("PRICE")
    private double price;
    /**
     * Year.
     */
    @JsonProperty("YEAR")
    private int year;

    /**
     * Default constructor.
     * needed for Jackson XML parsing
     */
    public CD() {
    }

    /**
     * Builder constructor.
     * @param cdBuilder
     */
    public CD(final CDBuilder cdBuilder) {
        if (cdBuilder == null) {
            throw new IllegalArgumentException(
                    "Please provide CDBuilder to build CD object."
            );
        }
        title = cdBuilder.title;
        artist = cdBuilder.artist;
        country = cdBuilder.country;
        company = cdBuilder.company;
        price = cdBuilder.price;
        year = cdBuilder.year;
    }

    /**
     * Builder.
     */
    public static final class CDBuilder {
        /**
         * Title.
         */
        private final String title;
        /**
         * Artist.
         */
        private final String artist;
        /**
         * Country.
         */
        private String country = "";
        /**
         * Company.
         */
        private String company = "";
        /**
         * Price.
         */
        private double price = 0D;
        /**
         * Year.
         */
        private int year = 0;

        /**
         * Builder constructor.
         * @param pTitle Title
         * @param pArtist Artist
         */
        public CDBuilder(final String pTitle, final String pArtist) {
            if (pTitle == null || pTitle.trim().isEmpty()) {
                throw new IllegalArgumentException(
                        "Please provide correct CD Title."
                );
            }
            if (pArtist == null || pArtist.trim().isEmpty()) {
                throw new IllegalArgumentException(
                        "Please provide correct CD Artist."
                );
            }
            title = pTitle;
            artist = pArtist;
        }

        /**
         * Setter.
         * @param pCountry Country
         * @return this
         */
        public CDBuilder setCountry(final String pCountry) {
            country = pCountry;
            return this;
        }

        /**
         * Setter.
         * @param pCompany Company
         * @return this
         */
        public CDBuilder setCompany(final String pCompany) {
            company = pCompany;
            return this;
        }

        /**
         * Setter.
         * @param pPrice Price
         * @return this
         */
        public CDBuilder setPrice(final double pPrice) {
            price = pPrice;
            return this;
        }

        /**
         * Setter.
         * @param pYear Year
         * @return this
         */
        public CDBuilder setYear(final int pYear) {
            year = pYear;
            return this;
        }

        /**
         * BuildMethod.
         * @return new CD
         */
        public CD build() {
            return new CD(this);
        }
    }

    /**
     * Getter.
     *
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Getter.
     *
     * @return artist name
     */
    public String getArtist() {
        return artist;
    }

    /**
     * Getter.
     *
     * @return country name
     */
    public String getCountry() {
        return country;
    }

    /**
     * Getter.
     *
     * @return price
     */
    public double getPrice() {
        return price;
    }

    /**
     * Getter.
     *
     * @return release year
     */
    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        String ttl = "title:";
        String artst = "artist:";
        String cntry = "country:";
        String cmpny = "company:";
        String prc = "price:";
        String yr = "year:";

        return String.format("CD: \n"
                        + "%1$8s %7$s\n"
                        + "%2$8s %8$s\n"
                        + "%3$8s %9$s\n"
                        + "%4$8s %10$s\n"
                        + "%5$8s %11$s\n"
                        + "%6$8s %12$s\n",
                ttl, artst, cntry, cmpny, prc, yr,
                title, artist, country, company, price, year);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CD cd = (CD) o;
        return Double.compare(cd.price, price) == 0
                && year == cd.year
                && title.equals(cd.title)
                && artist.equals(cd.artist)
                && country.equals(cd.country)
                && company.equals(cd.company);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, artist, country, company, price, year);
    }
}
