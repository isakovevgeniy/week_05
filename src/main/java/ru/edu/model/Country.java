package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
@JacksonXmlRootElement(localName = "Country")
public final class Country implements Serializable {

    /**
     * Country name.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;
    /**
     * List of artists from this country.
     */
    @JsonProperty()
    private List<Artist> artists = new ArrayList<>();

    /**
     * Constructor.
     * @param pName Country name
     */
    public Country(final String pName) {
        name = pName;
    }

    /**
     * Getter.
     * @return CountryName
     */
    public String getName() {
        return name;
    }

    /**
     * Getter.
     * @return List of artists from this country
     */
    @JsonProperty()
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Artist")
    public List<Artist> getArtists() {
        return artists;
    }

    /**
     * Setter.
     * @param pArtists - List of artists from this country
     */
    public void setArtists(final List<Artist> pArtists) {
        artists = pArtists;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Country country = (Country) o;
        return Objects.equals(name, country.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(name + "\n");
        sb.append("Artists: ");
        for (Artist artist : artists) {
            sb.append("\n").append(artist);
        }
        return sb.toString();
    }
}
