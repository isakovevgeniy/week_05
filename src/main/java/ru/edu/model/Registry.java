package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.List;

@JacksonXmlRootElement(localName = "ArtistRegistry")
public final class Registry implements Serializable {
    /**
     * Countries count.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "countryCount", isAttribute = true)
    private int countryCount;

    /**
     * List of countries.
     */
    @JsonProperty()
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Country")
    private List<Country> countries;

    /**
     * Simple constructor.
     * @param pCountries
     */
    public Registry(final List<Country> pCountries) {
        countries = pCountries;
        countryCount = pCountries.size();
    }

    /**
     * Getter.
     * @return int countryCount
     */
    public int getCountryCount() {
        return countryCount;
    }

    /**
     * Getter.
     * @return List<Country> countries
     */
    public List<Country> getCountries() {
        return countries;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Countries: ");
        for (Country s : countries) {
            sb.append("\n").append(s);
        }
        return sb.toString();
    }
}
