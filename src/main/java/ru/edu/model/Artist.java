package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public final class Artist implements Serializable {
    /**
     * Имя исполнителя.
     */
    @JsonProperty()
    private String name;

    /**
     * Список альбомов исполнителя.
     */
    @JsonProperty()
    private List<Album> albums = new ArrayList<>();

    /**
     * Конструткор.
     * @param pName
     */
    public Artist(final String pName) {
        name = pName;
    }

    /**
     * Геттер.
     * @return albums - Список альбомов исполнителя.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Albums")
    public List<Album> getAlbums() {
        return albums;
    }

    /**
     * Сеттер.
     * @param pAlbums - Список альбомов исполнителя.
     */
    public void setAlbums(final List<Album> pAlbums) {
        albums = pAlbums;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Artist artist = (Artist) o;
        return name.equals(artist.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(name + "\n");
        sb.append("Albums: ");
        for (Album album : albums) {
            sb.append("\n").append(album);
        }
        return sb.toString();
    }
}
