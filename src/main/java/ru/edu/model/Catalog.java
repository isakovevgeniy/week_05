package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "Catalog")
public final class Catalog implements Serializable {
    /**
     * Список объектов типа CD.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("CD")
    private List<CD> cd = new ArrayList<>();

    /**
     * Геттер.
     * @return CD - Список объектов типа CD.
     */
    @JsonProperty("CD")
    public List<CD> getCdList() {
        return cd;
    }

    /**
     * Setter.
     * @param pCD - yes it is
     */
    public void setCdList(final List<CD> pCD) {
        cd = pCD;
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder("Catalog: ");
        for (CD cd1 : cd) {
            output.append("\n").append(cd1);
        }
        return output.toString();
    }
}
