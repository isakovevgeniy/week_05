package ru.edu;

import ru.edu.model.Catalog;
import ru.edu.model.Registry;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public final class ByteRegistrySerializer implements RegistryExternalStorage {

    /**
     * Read file.
     * Parse file.
     * Create Catalog object.
     *
     * @param filePath - path to XML File
     * @return Catalog object
     */
    @Override
    public Catalog createCatalogFrom(final String filePath) {
        Catalog catalog = new Catalog();
        try (
                FileInputStream fis = new FileInputStream(filePath);
                ObjectInputStream ois = new ObjectInputStream(fis);
        ) {
            catalog = (Catalog) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return catalog;
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param registry реестр
     */
    @Override
    public void writeTo(final String filePath, final Registry registry) {
        try (
                FileOutputStream fis = new FileOutputStream(filePath);
                ObjectOutputStream ois = new ObjectOutputStream(fis);
        ) {
            ois.writeObject(registry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
