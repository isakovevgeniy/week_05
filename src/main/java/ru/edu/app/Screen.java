package ru.edu.app;

import ru.edu.ByteRegistrySerializer;
import ru.edu.JSONRegistrySerializer;
import ru.edu.RegistryExternalStorage;
import ru.edu.XMLRegistrySerializer;

import java.util.Scanner;

import static ru.edu.app.FileFormat.JAVA;
import static ru.edu.app.FileFormat.JSON;
import static ru.edu.app.FileFormat.XML;

public abstract class Screen {
    /**
     * Строка из консоли.
     */
    private String string;
    /**
     * Экран.
     */
    private String screen;
    /**
     * Сканнер.
     */
    private Scanner scanner = new Scanner(System.in);

    /**
     * Создает объект экран.
     * @param pScreen экран в строковом выражении
     */
    public Screen(final String pScreen) {
        screen = pScreen;
    }

    /**
     * Строка из консоли.
     * @return строка
     */
    public String getLine() {
        return string;
    }

    /**
     * Читает строки из консоли.
     * @return line строка из консоли
     */
    public String reader() {
        System.out.print("Введите значение: ");
        string = scanner.nextLine();
        return string;
    }

    /**
     * Парсит тип файла из введенной строки с именем файла.
     * @param line строка из консоли
     * @return типа файла
     */
    public FileFormat lineParser(final String line) {
        String fileFormat = line.substring(line.lastIndexOf(".") + 1);
        switch (fileFormat) {
            case("json"):
                return JSON;
            case("xml"):
                return XML;
            case("serialized"):
                return JAVA;
            default: throw new IllegalArgumentException("Неизвестный формат");
        }
    }

    /**
     * Вывод на экран.
     */
    public void print() {
        System.out.print(screen);
    }

    /**
     * Создает объект сериализатора.
     * @param fileFormat типа сериализуемого файла
     * @return сериализатор
     */
    public RegistryExternalStorage getSerializer(final FileFormat fileFormat) {
        switch (fileFormat) {
            case JSON:
                return new JSONRegistrySerializer();
            case XML:
                return new XMLRegistrySerializer();
            case JAVA:
                return new ByteRegistrySerializer();
            default: throw new IllegalArgumentException("Неизвестный формат");
        }
    }

    /**
     * Делает все, что нужно.
     */
    public abstract void getSmth();
}
