package ru.edu.app;

import ru.edu.RegistryFromCatalogCreator;
import ru.edu.model.Catalog;
import ru.edu.model.Registry;

public class Application {
    /**
     * Счетчик циклов смены экранов.
     */
    private static int i;
    /**
     * Формат исходного файла.
     */
    private FileFormat srcFileFormat;
    /**
     * Формат файла с руезультатами.
     */
    private FileFormat dstFileFormat;
    /**
     * Путь к исходному файлу.
     */
    private String srcPath;
    /**
     * Путь к файлу с результатами.
     */
    private String dstPath;
    /**
     * Первый экран. Приветствия.
     */
    private final String screen0 =
            "\n"
            + "Создать реестр исполнителей из каталога\n"
            + "1. Продолжить\n"
            + "N. Выйти\n";
    /**
     * Второй экран. Выбор исходного файла.
     */
    private final String screen1 =
            "\n"
            + "Файлы каталогов поумолчанию:\n"
            + "1. \"JAVA\" - ./src/test/resources/TestByteCatalog.serialized\n"
            + "2. \"JSON\" - ./src/test/resources/TestJSONCatalog.json\n"
            + "3. \"XML\"  - ./src/test/resources/TestXMLCatalog.xml\n"
            + "4. Ввести другой путь\n"
            + "N. Выход\n";
    /**
     * Третий экран. Выбор файла с результатом.
     */
    private final String screen2 =
            "\n"
            + "Файлы сохранения реестра артистов поумолчанию:\n"
            + "1. \"JAVA\" - ./output/artist_by_country.serialized\n"
            + "2. \"JSON\" - ./output/artist_by_country.json\n"
            + "3. \"XML\"  - ./output/artist_by_country.xml\n"
            + "4. Ввести другой путь\n"
            + "5. Вернуться к предыдущему экрану\n"
            + "N. Выход\n";
    /**
     * Четвертый экран. Контроль. Возможность передумать.
     */
    private final String screen3 =
            "\n"
            + "1. Создать реестр\n"
            + "2. Вернуться к предыдущему экрану\n"
            + "N. Выход\n";
    /**
     * Пятый экран. Сообщение о выполнении.
     */
    private final String screen4 =
            "\nСоздание реестра выполнено\n"
            + "N. Выход\n";
    /**
     * Массив экранов.
     */
    private final Screen[] screens = {
            new Screen(screen0) {
                @Override
                public void getSmth() {
                    if (!getLine().equals("1")) {
                        System.out.println("Введено некорректное значение");
                        i--;
                    }
                }
            },
            new Screen(screen1) {
                @Override
                public void getSmth() {
                    switch (getLine()) {
                        case "1":
                            srcPath = "./src/test/resources/"
                                    + "TestByteCatalog.serialized";
                            srcFileFormat = FileFormat.JAVA;
                            break;
                        case "2":
                            srcPath = "./src/test/resources/"
                                    + "TestJSONCatalog.json";
                            srcFileFormat = FileFormat.JSON;
                            break;
                        case "3":
                            srcPath = "./src/test/resources/"
                                    + "TestXMLCatalog.xml";
                            srcFileFormat = FileFormat.XML;
                            break;
                        case "4":
                            reader();
                            srcPath = getLine();
                            srcFileFormat = lineParser(getLine());
                            break;
                        default:
                            System.out.println("Введено некорректное значение");
                            i--;
                            break;
                    }
                    System.out.print("\n"
                            + "Файл-источник:" + srcPath + "\n"
                            + "Формат файла-источника:" + srcFileFormat);
                }
            },
            new Screen(screen2) {
                @Override
                public void getSmth() {
                    switch (getLine()) {
                        case "1":
                            dstPath = "./output/artist_by_country.serialized";
                            dstFileFormat = FileFormat.JAVA;
                            break;
                        case "2":
                            dstPath = "./output/artist_by_country.json";
                            dstFileFormat = FileFormat.JSON;
                            break;
                        case "3":
                            dstPath = "./output/artist_by_country.xml";
                            dstFileFormat = FileFormat.XML;
                            break;
                        case "4":
                            reader();
                            dstPath = getLine();
                            dstFileFormat = lineParser(getLine());
                            break;
                        case "5":
                            i -= 2;
                            return;
                        default:
                            System.out.println("Введено некорректное значение");
                            i--;
                            break;
                    }
                    System.out.print("\n"
                            + "Файл-источник:" + srcPath + "\n"
                            + "Формат файла-источника:" + srcFileFormat + "\n"
                            + "Файл-результатов:" + dstPath + "\n"
                            + "Формат файла-результатов:" + dstFileFormat);
                }
            },
            new Screen(screen3) {
                @Override
                public void getSmth() {
                    switch (getLine()) {
                        case "1":
                            Catalog catalog = getSerializer(srcFileFormat)
                                    .createCatalogFrom(srcPath);
                            Registry registry = new RegistryFromCatalogCreator(
                                    catalog
                            ).getRegistry();
                            getSerializer(dstFileFormat)
                                    .writeTo(dstPath, registry);
                            break;
                        case "2":
                            i -= 2;
                            return;
                        default:
                            System.out.println("Введено некорректное значение");
                            i--;
                            break;
                    }
                }
            },
            new Screen(screen4) {
                @Override
                public void getSmth() {
                    i = -1;
                }
            } };

    /**
     * Запуск приложения.
     *
     * @param args - не требуются.
     */
    public static void main(final String[] args) {
        Application app = new Application();
        for (; i < app.screens.length; i++) {
            app.screens[i].print();
            if (app.screens[i].reader().equalsIgnoreCase("n")) {
                return;
            }
            app.screens[i].getSmth();
        }
    }
}
