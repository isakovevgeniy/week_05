package ru.edu.app;

import ru.edu.ByteRegistrySerializer;
import ru.edu.JSONRegistrySerializer;
import ru.edu.RegistryExternalStorage;
import ru.edu.RegistryFromCatalogCreator;
import ru.edu.XMLRegistrySerializer;
import ru.edu.model.Catalog;
import ru.edu.model.Registry;

public class ConsoleApplication {
    /**
     *
     */
    private static final String JSON = "json";
    /**
     *
     */
    private static final String XML = "xml";
    /**
     *
     */
    private static final String JAVA = "serialized";

    /**
     *
     * @param args парамаргс.
     */
    public static void main(final String[] args) {
        ConsoleApplication app = new ConsoleApplication();
        String srcPath = app.pathParser(args[0]);
        String dstPath = app.pathParser(args[1]);
        String srcFileFormat = app.lineParser(srcPath);
        String dstFileFormat = app.lineParser(dstPath);

        Catalog catalog = app.getSerializer(srcFileFormat)
                .createCatalogFrom(srcPath);
        Registry registry = new RegistryFromCatalogCreator(
                catalog
        ).getRegistry();
        app.getSerializer(dstFileFormat)
                .writeTo(dstPath, registry);
        System.out.println("Запись каталога из "
                + srcPath
                + " в "
                + dstPath
                + " успешно завершена");
    }

    /**
     * Парсит тип путь к файлу.
     * @param line строка из консоли
     * @return типа файла
     */
    public String pathParser(final String line) {
        return line.substring(line.indexOf("=") + 1);
    }
    /**
     * Парсит тип файла из введенной строки с именем файла.
     * @param line строка из консоли
     * @return типа файла
     */
    public String lineParser(final String line) {
        String fileFormat = line.substring(line.lastIndexOf(".") + 1);
        switch (fileFormat) {
            case("json"):
                return JSON;
            case("xml"):
                return XML;
            case("serialized"):
                return JAVA;
            default: throw new IllegalArgumentException("Неизвестный формат");
        }
    }
    /**
     * Создает объект сериализатора.
     * @param fileFormat типа сериализуемого файла
     * @return сериализатор
     */
    public RegistryExternalStorage getSerializer(final String fileFormat) {
        switch (fileFormat) {
            case JSON:
                return new JSONRegistrySerializer();
            case XML:
                return new XMLRegistrySerializer();
            case JAVA:
                return new ByteRegistrySerializer();
            default: throw new IllegalArgumentException("Неизвестный формат");
        }
    }
}
