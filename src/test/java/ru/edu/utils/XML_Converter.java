package ru.edu.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.edu.XMLRegistrySerializer;
import ru.edu.model.Catalog;

import java.io.*;

public class XML_Converter {
    private final static Catalog catalog = new XMLRegistrySerializer().createCatalogFrom("./src/test/resources/TestXMLCatalog.xml");
    private File file;

    public XML_Converter(String outputFilePath){
        this.file = new File(outputFilePath);
    }

    public static void main(String[] args) {
        XML_Converter xml_to_byte_converter = new XML_Converter("./src/test/resources/TestByteCatalog.serialized");
        xml_to_byte_converter.writeToByte();

        XML_Converter xml_to_JSON_converter = new XML_Converter("./src/test/resources/TestJSONCatalog.json");
        xml_to_JSON_converter.writeToJSON();
    }

    private void writeToByte() {
        try (
                FileOutputStream fis = new FileOutputStream(file);
                ObjectOutputStream ois = new ObjectOutputStream(fis);
        ) {
            ois.writeObject(catalog);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void writeToJSON() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(file, catalog);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

//<?xml version="1.0" encoding="UTF-8"?>
//<CATALOG>
//<CD>
//<TITLE>Empire Burlesque</TITLE>
//<ARTIST>Bob Dylan</ARTIST>
//<COUNTRY>USA</COUNTRY>
//<COMPANY>Columbia</COMPANY>
//<PRICE>10.90</PRICE>
//<YEAR>1985</YEAR>
//</CD>
//<CD>
//<TITLE>Hide your heart</TITLE>
//<ARTIST>Bonnie Tyler</ARTIST>
//<COUNTRY>UK</COUNTRY>
//<COMPANY>CBS Records</COMPANY>
//<PRICE>9.90</PRICE>
//<YEAR>1988</YEAR>
//</CD>
//<CD>
//<TITLE>Greatest Hits</TITLE>
//<ARTIST>Dolly Parton</ARTIST>
//<COUNTRY>USA</COUNTRY>
//<COMPANY>RCA</COMPANY>
//<PRICE>9.90</PRICE>
//<YEAR>1982</YEAR>
//</CD>
//</CATALOG>
