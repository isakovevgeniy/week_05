package ru.edu;

import org.junit.Test;

public class JSONRegistrySerializerTest extends RegistrySerializerTest{

    @Test
    public void test() {
        inputFilePath = "./src/test/resources/TestJSONCatalog.json";
        outputFilePath = "./output/artist_by_country.json";
        serializer = new JSONRegistrySerializer();

        createCatalogFromTest(serializer);
        writeToTest();
    }
}
