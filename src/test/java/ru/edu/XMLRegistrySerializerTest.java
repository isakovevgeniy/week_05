package ru.edu;

import org.junit.Test;

public class XMLRegistrySerializerTest extends RegistrySerializerTest{

    @Test
    public void test() {
        inputFilePath = "./src/test/resources/TestXMLCatalog.xml";
        outputFilePath = "./output/artist_by_country.xml";
        serializer = new XMLRegistrySerializer();

        createCatalogFromTest(serializer);
        writeToTest();
    }
}
