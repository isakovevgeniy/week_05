package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CatalogTest {
    Catalog catalog = new Catalog();
    CD cd;
    List<CD> cdList;

    @Before
    public void setup() {
        cd = new CD.CDBuilder("title", "artist")
                .setCompany("company")
                .setCountry("country")
                .setPrice(100)
                .setYear(2000)
                .build();
        cdList = new ArrayList<>();
        catalog.setCdList(cdList);
        catalog.getCdList().add(cd);
    }

    @Test
    public void getCdListTest() {
        assertEquals(cdList, catalog.getCdList());
    }

    @Test
    public void setCdList() {
        assertEquals(cdList, catalog.getCdList());
    }

    @Test
    public void toStringTest() {
        String line = "Catalog: "
                + "\n"
                + cd.toString();
        assertEquals(line, catalog.toString());
    }
}
