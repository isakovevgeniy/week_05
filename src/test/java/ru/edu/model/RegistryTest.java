package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class RegistryTest {
    Country country;
    List<Country> countries;
    Registry registry;
    String string;

    @Before
    public void setup() {
        country = new Country("Russia");
        countries = new ArrayList<>();
        countries.add(country);
        registry = new Registry(countries);
        string = "Countries: \n"
                + "Russia\n"
                + "Artists: ";
    }

    @Test
    public void testToString() {
        assertEquals(string, registry.toString());
    }
}