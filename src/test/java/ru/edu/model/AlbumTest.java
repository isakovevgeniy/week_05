package ru.edu.model;

import org.junit.Test;
import static org.junit.Assert.*;

public class AlbumTest {
    Album album = new Album("album", 1900);
    Album albumCopy = new Album("album", 1900);
    Album anotherAlbum = new Album("anotherAlbum", 1900);
    Album anotherAlbum_2 = new Album("album", 2000);

    @Test
    public void equalsTest(){
        assertEquals(album, album);
        assertEquals(album, albumCopy);
        assertNotEquals(album, anotherAlbum);
        assertNotEquals(album, anotherAlbum_2);
        assertNotEquals(album, null);
        assertNotEquals(album, "null");
    }

    @Test
    public void hashCodeTest(){
        assertEquals(album.hashCode(), albumCopy.hashCode());
        assertNotEquals(album.hashCode(), anotherAlbum.hashCode());
    }

    @Test
    public void toStringTest(){
        String line = "Name: " + "album" + " Year: " + 1900;
        assertEquals(line, album.toString());
    }
}
