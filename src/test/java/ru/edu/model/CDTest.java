package ru.edu.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CDTest {
    CD cd;
    CD cdCopy;
    CD anotherCd;
    String string;

    @Before
    public void setUp() throws Exception {
        cd = new CD.CDBuilder("title", "artist")
                .setCountry("country")
                .setCompany("company")
                .setPrice(100)
                .setYear(2000)
                .build();
        cdCopy = new CD.CDBuilder("title", "artist")
                .setCountry("country")
                .setCompany("company")
                .setPrice(100)
                .setYear(2000)
                .build();
        anotherCd = new CD.CDBuilder("title", "artist")
                .build();

        string = String.format("CD: \n"
                        + "%1$8s %7$s\n"
                        + "%2$8s %8$s\n"
                        + "%3$8s %9$s\n"
                        + "%4$8s %10$s\n"
                        + "%5$8s %11$s\n"
                        + "%6$8s %12$s\n",
                "title:", "artist:", "country:", "company:", "price:", "year:",
                "title", "artist", "country", "company", 100d, 2000);
    }
    @Test
    public void putToConstructorNullParametersTest(){
        try {
            new CD(null);
            Assert.fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            assertEquals("Please provide CDBuilder to build CD object.", e.getMessage());
        }
    }
    @Test
    public void getTitleTest() {
        assertEquals("title", cd.getTitle());
    }

    @Test
    public void getArtistTest() {
        assertEquals("artist", cd.getArtist());
    }

    @Test
    public void getCountryTest() {
        assertEquals("country", cd.getCountry());
    }

    @Test
    public void getPriceTest() {
        assertEquals(0, Double.compare(100, cd.getPrice()));
    }

    @Test
    public void getYearTest() {
        assertEquals(2000, cd.getYear());
    }

    @Test
    public void testToStringTest() {
        assertEquals(string, cd.toString());
    }

    @Test
    public void testEqualsTest() {
        assertEquals(cd, cd);
        assertEquals(cd, cdCopy);
        assertNotEquals(cd, anotherCd);
        assertNotEquals(cd, null);
        assertNotEquals(cd, "otherCd");

    }

    @Test
    public void testHashCodeTest() {
        assertEquals(cd.hashCode(), cdCopy.hashCode());
        assertNotEquals(cd.hashCode(), anotherCd.hashCode());
    }
}