package ru.edu.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CDBuilderTest {
    CD cd;

    @Before
    public void setUp() throws Exception {
        cd = new CD.CDBuilder("title", "artist")
                .setCountry("country")
                .setCompany("company")
                .setPrice(100)
                .setYear(2000)
                .build();
    }
    @Test
    public void putToBuilderNullTitleTest(){
        try {
            new CD.CDBuilder(null, "artistName");
            Assert.fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            assertEquals("Please provide correct CD Title.", e.getMessage());
        }
    }
    @Test
    public void putToBuilderNullArtistTest(){
        try {
            new CD.CDBuilder("CDTitle", null);
            Assert.fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            assertEquals("Please provide correct CD Artist.", e.getMessage());
        }
    }

    @Test
    public void putToBuilderEmptyTitleTest(){
        try {
            new CD.CDBuilder("", "artistName");
            Assert.fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            assertEquals("Please provide correct CD Title.", e.getMessage());
        }
    }
    @Test
    public void putToBuilderEmptyArtistTest(){
        try {
            new CD.CDBuilder("CDTitle", "");
            Assert.fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            assertEquals("Please provide correct CD Artist.", e.getMessage());
        }
    }
    @Test
    public void setTitleTest() {
        assertEquals("title", cd.getTitle());
    }

    @Test
    public void setArtistTest() {
        assertEquals("artist", cd.getArtist());
    }

    @Test
    public void setCountryTest() {
        assertEquals("country", cd.getCountry());
    }

    @Test
    public void setPriceTest() {
        assertEquals(0, Double.compare(100, cd.getPrice()));
    }

    @Test
    public void setYearTest() {
        assertEquals(2000, cd.getYear());
    }

}