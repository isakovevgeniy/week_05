package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CountryTest {
    Country country;
    Country countryCopy;
    Country anotherCountry;
    List<Artist> artists;
    String string;

    @Before
    public void setUp() throws Exception {
        country = new Country("Russia");
        countryCopy = new Country("Russia");
        anotherCountry = new Country("OtherCountry");
        artists = new ArrayList<>();
        artists.add(new Artist("artist"));
        country.setArtists(artists);
        string ="Russia\n"
                + "Artists: \n"
                + "artist\n"
                + "Albums: ";
    }

    @Test
    public void getArtists() {
        assertEquals(artists, country.getArtists());
    }

    @Test
    public void setArtists() {
        assertEquals(artists, country.getArtists());
    }

    @Test
    public void testEquals() {
        assertEquals(country, country);
        assertEquals(country, countryCopy);
        assertNotEquals(country, anotherCountry);
        assertNotEquals(country, null);
        assertNotEquals(country, "anotherCountry");
    }

    @Test
    public void testHashCode() {
        assertEquals(country.hashCode(), countryCopy.hashCode());
        assertNotEquals(country.hashCode(), anotherCountry.hashCode());
    }

    @Test
    public void testToString() {
        assertEquals(string, country.toString());
    }
}