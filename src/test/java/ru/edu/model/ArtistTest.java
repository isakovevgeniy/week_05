package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ArtistTest {
    Artist artist = new Artist("artist");
    Artist artistCopy = new Artist("artist");
    Artist anotherArtist = new Artist("anotherArtist");
    Album album;
    List<Album> albums;

    @Before
    public void setup() {
        album = new Album("album", 1900);
        albums = new ArrayList<>();
        albums.add(album);
        artist.setAlbums(albums);

    }

    @Test
    public void getAlbumsTest(){
        assertEquals(albums, artist.getAlbums());
    }

    /**
     * Вопрос по корректности проверки Геттеров и Сеттеров
     */
    @Test
    public void setAlbumsTest(){
        assertEquals(albums, artist.getAlbums());
    }

    @Test
    public void equalsTest(){
        assertEquals(artist, artist);
        assertEquals(artist, artistCopy);
        assertNotEquals(artist, anotherArtist);
        assertNotEquals(artist, null);
        assertNotEquals(artist, "anotherArtist");

    }

    @Test
    public void hashCodeTest(){
        assertEquals(artist.hashCode(), artistCopy.hashCode());
        assertNotEquals(artist.hashCode(), anotherArtist.hashCode());
    }

    @Test
    public void toStringTest(){
        String line = "artist"
                + "\n"
                + "Albums: "
                + "\n"
                + "Name: " + "album" + " Year: " + 1900;
        assertEquals(line, artist.toString());
    }
}
