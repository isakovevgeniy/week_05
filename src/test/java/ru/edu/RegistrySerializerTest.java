package ru.edu;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.edu.model.CD;
import ru.edu.model.Catalog;
import ru.edu.model.Registry;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class RegistrySerializerTest {
    Registry registry;
    Catalog catalog;
    RegistryExternalStorage serializer;
    String inputFilePath;
    String outputFilePath;

    public void createCatalogFromTest(RegistryExternalStorage serializer) {
        catalog = serializer.createCatalogFrom(inputFilePath);
        Assert.assertEquals(4, catalog.getCdList().size());
        CD firstElement = new CD.CDBuilder("Empire Burlesque", "Bob Dylan")
                .setCountry("USA")
                .setCompany("Columbia")
                .setPrice(10.90)
                .setYear(1985)
                .build();
        Assert.assertEquals(firstElement,catalog.getCdList().get(0));
        Assert.assertEquals("Bonnie Tyler", catalog.getCdList().get(1).getArtist());
        Assert.assertEquals("Greatest Hits", catalog.getCdList().get(2).getTitle());

        RegistryFromCatalogCreator creator = new RegistryFromCatalogCreator(catalog);
        registry = creator.getRegistry();

        System.out.println(serializer.getClass().getSimpleName() + "createCatalogFromTest completed successfully");
    }


    public void writeToTest() {
        createCatalogFromTest(serializer);
        Path output = Paths.get(outputFilePath);
        try {
            Files.deleteIfExists(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertFalse(Files.exists(output));
        serializer.writeTo(outputFilePath, registry);
        Assert.assertNotNull(readFrom(outputFilePath));

        System.out.println(serializer.getClass().getSimpleName() + "writeToTest completed successfully");
    }

    private String readFrom(String path){
        StringBuilder sb = new StringBuilder();
        try(
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path)));) {
            while (br.ready()) {
                sb.append(br.readLine()).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
