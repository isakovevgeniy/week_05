package ru.edu;

import org.junit.Test;

public class ByteRegistrySerializerTest extends RegistrySerializerTest{

    @Test
    public void test() {
        inputFilePath = "./src/test/resources/TestByteCatalog.serialized";
        outputFilePath = "./output/artist_by_country.serialized";
        serializer = new ByteRegistrySerializer();

        createCatalogFromTest(serializer);
        writeToTest();
    }
}
