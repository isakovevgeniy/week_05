package ru.edu;

import org.junit.Assert;
import org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import ru.edu.model.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RegistryFromCatalogCreatorTest {
    CD cd;
    List<CD> cdList;
    Catalog catalog;
    RegistryFromCatalogCreator creator;
    Registry registry;

    List<Country> countries;
    Country country;
    Artist artist;
    Album album;

    @Before
    public void setup() {
        //Creating test catalog
        cd = cd = new CD.CDBuilder("titleName", "artistName")
                .setCountry("countryName")
                .setCompany("companyName")
                .setPrice(100)
                .setYear(2000)
                .build();
        cdList = new ArrayList<>();
        cdList.add(cd);
        cdList.add(cd);
        catalog = new Catalog();

        catalog.setCdList(cdList);

        //Creating test registry
        country = new Country("countryName");
        artist = new Artist("artistName");
        album = new Album("titleName", 2000);

        artist.setAlbums(new ArrayList<>());
        artist.getAlbums().add(album);
        country.setArtists(new ArrayList<>());
        country.getArtists().add(artist);
        countries = new ArrayList<>();
        countries.add(country);

        registry = new Registry(countries);

    }
    @Test
    public void putToConstructorListAsNullTest(){
        try {
            new RegistryFromCatalogCreator((List<CD>) null);
            Assert.fail("Expected NullPointerException");
        } catch (NullPointerException e) {
            assertEquals("Catalog = null. Please provide correct CD catalog.", e.getMessage());
        }
    }
    @Test
    public void putToConstructorCatalogAsNullTest(){
    //не нашел способ обработать Null в конструткоре
    }

    @Test
    public void getRegistryFromCatalogTest() {
        creator = new RegistryFromCatalogCreator(catalog);
        Registry testRegistry = creator.getRegistry();
        assertEquals(registry.getCountryCount(), testRegistry.getCountryCount());
        assertEquals(registry.getCountries(), testRegistry.getCountries());
    }
}

